#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <fcntl.h> //文件控制定义
#include <termios.h>//终端控制定义
#include <strings.h>
#include <string.h>
#include <pthread.h>

#define SER_DEV_PATH "/dev/ttySAC3" // 打开第一组串口


	int ser_fd = -1;  // 串口文件描述符
	char buf[1024] = {0};
	char buf1[1024] = {0};
	
	
void init_tty(int infra_fd)
{    
	//声明设置串口的结构体
	struct termios termios_new;
	//先清空该结构体
	bzero( &termios_new, sizeof(termios_new));
	//	cfmakeraw()设置终端属性，就是设置termios结构中的各个参数。
	cfmakeraw(&termios_new);
	//设置波特率
	termios_new.c_cflag=(B9600);
	//CLOCAL和CREAD分别用于本地连接和接受使能，因此，首先要通过位掩码的方式激活这两个选项。    
	termios_new.c_cflag |= CLOCAL | CREAD;
	//通过掩码设置数据位为8位
	termios_new.c_cflag &= ~CSIZE;
	termios_new.c_cflag |= CS8; 
	//设置无奇偶校验
	termios_new.c_cflag &= ~PARENB;
	//一位停止位
	termios_new.c_cflag &= ~CSTOPB;
	tcflush(infra_fd,TCIFLUSH);
	// 可设置接收字符和等待时间，无特殊要求可以将其设置为0
	termios_new.c_cc[VTIME] = 10;//等待时间
	termios_new.c_cc[VMIN] = 1;//设置最小字符
	// 用于清空输入/输出缓冲区
	tcflush (infra_fd, TCIFLUSH);
	//完成配置后，可以使用以下函数激活串口设置
	if(tcsetattr(infra_fd,TCSANOW,&termios_new) )
		printf("Setting the serial1 failed!\n");

}

/* void *func1(void *arg)
{
	
	// 写数据
		printf(" Please enter the words:\n");
		scanf("%s",buf);
		int ret =write(ser_fd, buf, sizeof(buf));
		printf("ret = %d",ret);
	
}
void *func2(void *arg)
{
	
	//读取串口数据
		fcntl(ser_fd, F_SETFL,0);
		int ret1 = read(ser_fd, buf1, sizeof(buf1));
		//memset(buf1,0,strlen(buf1));
		printf("read: %s\n", buf1);
		
	
} */

int main(int argc, char **argv)
{
	pthread_t tid1,tid2;
	ser_fd = open(SER_DEV_PATH, O_RDWR|O_NOCTTY);
	if(ser_fd < 0)
	{
			perror("ser_fd");
			exit(1);
	}
	int ret1 = isatty(ser_fd);
	printf("ret1 = %d\n",ret1);

	//初始化
	init_tty(ser_fd);
	printf("ser_fd=%d\n", ser_fd);
	while(1)
	{
		
		//读取串口数据
		fcntl(ser_fd, F_SETFL,0);
		int ret1 = read(ser_fd, buf1, sizeof(buf1));
		printf("read: %s\n", buf1);
		/* pthread_create(&tid1,NULL,func1,NULL);//创建线程
		pthread_create(&tid2,NULL,func2,NULL);//创建线程 */
		
		//pthread_join(tid2,NULL);//接合指定线程（先执行线程）
		memset(buf1,0,strlen(buf1));
	}
	close(ser_fd);
	return 0;
}
	