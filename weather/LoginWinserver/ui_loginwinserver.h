/********************************************************************************
** Form generated from reading UI file 'loginwinserver.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINWINSERVER_H
#define UI_LOGINWINSERVER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoginWinserver
{
public:
    QWidget *centralWidget;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *userEdit_2;
    QLineEdit *passEdit_2;
    QWidget *layoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *loginBt_2;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *quitBt_2;
    QLabel *label;
    QLabel *label_2;

    void setupUi(QMainWindow *LoginWinserver)
    {
        if (LoginWinserver->objectName().isEmpty())
            LoginWinserver->setObjectName(QStringLiteral("LoginWinserver"));
        LoginWinserver->resize(433, 268);
        LoginWinserver->setMinimumSize(QSize(433, 268));
        LoginWinserver->setMaximumSize(QSize(433, 374));
        LoginWinserver->setStyleSheet(QStringLiteral("background-image: url(:/new/prefix1/tupian/14.jpg);"));
        centralWidget = new QWidget(LoginWinserver);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(100, 100, 241, 71));
        verticalLayout_2 = new QVBoxLayout(layoutWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        userEdit_2 = new QLineEdit(layoutWidget);
        userEdit_2->setObjectName(QStringLiteral("userEdit_2"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(userEdit_2->sizePolicy().hasHeightForWidth());
        userEdit_2->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(16);
        userEdit_2->setFont(font);

        verticalLayout_2->addWidget(userEdit_2);

        passEdit_2 = new QLineEdit(layoutWidget);
        passEdit_2->setObjectName(QStringLiteral("passEdit_2"));
        sizePolicy.setHeightForWidth(passEdit_2->sizePolicy().hasHeightForWidth());
        passEdit_2->setSizePolicy(sizePolicy);
        passEdit_2->setFont(font);
        passEdit_2->setEchoMode(QLineEdit::Password);

        verticalLayout_2->addWidget(passEdit_2);

        layoutWidget_2 = new QWidget(centralWidget);
        layoutWidget_2->setObjectName(QStringLiteral("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(120, 210, 201, 31));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget_2);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        loginBt_2 = new QPushButton(layoutWidget_2);
        loginBt_2->setObjectName(QStringLiteral("loginBt_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(loginBt_2->sizePolicy().hasHeightForWidth());
        loginBt_2->setSizePolicy(sizePolicy1);
        loginBt_2->setFont(font);

        horizontalLayout_2->addWidget(loginBt_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        quitBt_2 = new QPushButton(layoutWidget_2);
        quitBt_2->setObjectName(QStringLiteral("quitBt_2"));
        sizePolicy1.setHeightForWidth(quitBt_2->sizePolicy().hasHeightForWidth());
        quitBt_2->setSizePolicy(sizePolicy1);
        quitBt_2->setFont(font);

        horizontalLayout_2->addWidget(quitBt_2);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 0, 431, 61));
        QFont font1;
        font1.setPointSize(36);
        label->setFont(font1);
        label->setAlignment(Qt::AlignCenter);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(0, -150, 441, 571));
        label_2->setStyleSheet(QStringLiteral("image: url(:/new/prefix1/tupian/14.jpg);"));
        LoginWinserver->setCentralWidget(centralWidget);
        label_2->raise();
        layoutWidget->raise();
        layoutWidget_2->raise();
        label->raise();

        retranslateUi(LoginWinserver);

        QMetaObject::connectSlotsByName(LoginWinserver);
    } // setupUi

    void retranslateUi(QMainWindow *LoginWinserver)
    {
        LoginWinserver->setWindowTitle(QApplication::translate("LoginWinserver", "LoginWinserver", 0));
        userEdit_2->setPlaceholderText(QApplication::translate("LoginWinserver", "\347\224\250\346\210\267", 0));
        passEdit_2->setPlaceholderText(QApplication::translate("LoginWinserver", "\345\257\206\347\240\201", 0));
        loginBt_2->setText(QApplication::translate("LoginWinserver", "\347\231\273\345\275\225", 0));
        quitBt_2->setText(QApplication::translate("LoginWinserver", "\351\200\200\345\207\272", 0));
        label->setText(QApplication::translate("LoginWinserver", "\345\220\216\345\217\260\347\256\241\347\220\206\346\216\247\345\210\266\347\263\273\347\273\237", 0));
        label_2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class LoginWinserver: public Ui_LoginWinserver {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINWINSERVER_H
