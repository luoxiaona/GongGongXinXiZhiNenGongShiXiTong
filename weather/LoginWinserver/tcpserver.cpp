﻿#include "tcpserver.h"
#include "ui_tcpserver.h"
#include <QMessageBox>
#include <QDebug>
#include "loginwinserver.h"
#include "databaseshow.h"


QSqlDatabase database;

TcpServer::TcpServer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TcpServer)
{
    ui->setupUi(this);
   // QPixmap *pix=new QPixmap(this->size());//size(0当前客户区的大小(例子见DrawWidgets:)
    //pix->fill(Qt::black);
   // time_curr = QDate::currentDate().toString("yyyy-MM-dd");
    //初始化QTcpServer对象
    mserver = new QTcpServer();
    msocket = new QTcpSocket();
    //关联客户端链接信号
    connect(mserver,SIGNAL(newConnection()),this, SLOT(new_client()));
    ui->sendBt->setEnabled(false);
    //添加数据库驱动、设置数据库名称、数据库登录用户名、密码
    database = QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName("../sqlite3/client.db");
    //打开数据库
    if(!database.open())
    {
        qDebug()<<database.lastError();
        qFatal("failed to connect.");
    }
    else
    {
        qDebug()<<"connected successfully";
    }

}

TcpServer::~TcpServer()
{
    delete ui;
}

/*开启服务器*/
void TcpServer::on_startBt_clicked()
{
    //检测是否已经监听
    if(mserver->isListening())
    {
        //如果监听就关闭
        mserver->close();
        ui->startBt->setText("开启");
        ui->portEdit->setEnabled(true);
        return ;
    }
    if(ui->portEdit->text().isEmpty())
    {
        //提示输入端口号
        QMessageBox::warning(this, "提示","请输入端口号");
        return ;
    }
    bool flag = mserver->listen(QHostAddress::Any,ui->portEdit->text().toShort());
    if(flag)
    {
        //监听成功
        ui->startBt->setText("关闭");
        ui->portEdit->setEnabled(false);
    }
}

/*客户端链接，触发槽函数， 接受链接，得到与客户端通信的套接字（QTcpSocket）*/
void TcpServer::new_client()
{
    qDebug()<<"新客户端链接";
    msocket = mserver->nextPendingConnection();
    //给客户端发送数据
    msocket->write("欢迎使用本公司产品 如有合作意向 请联系电话：13678XXXXXX");
    //关联读数据信号
    connect(msocket,SIGNAL(readyRead()),this, SLOT(read_data()));
    //客户端掉线会发送disconnected信号
    connect(msocket,SIGNAL(disconnected()),this,SLOT(client_dis()));
    //获取客户端IP地址
    QString ip = msocket->peerAddress().toString();
    ui->listWidget->addItem(ip);
    //把客户端套接字添加到列表sockets中
    sockets.append(msocket);
    //启用发送按钮
    ui->sendBt->setEnabled(true);
    ui->listWidget->setCurrentRow(0);
}

/*发送数据*/
void TcpServer::on_sendBt_clicked()
{
    //获取选择的客户端
    int row = ui->listWidget->currentRow();
    QString msg = ui->sendEdit->toPlainText().toUtf8();
    sockets.at(row)->write(msg.toUtf8());
}

/*读取数据信息并存储到数据库*/
void TcpServer::read_data()
{
    //信号发送者--掉线客户端套接字
    QTcpSocket *sock = (QTcpSocket*)sender();
    QString msg = sock->readAll();
   // ui->textBrowser->insertPlainText(msg);
    qDebug()<<"recv:"<<msg;
    QString ip = sock->peerAddress().toString();
    //ui->recvList->addItem(ip);

    QString date        = msg.section('=',0,0);
    QString time        = msg.section('=',1,1);
    QString temperature = msg.section('=',2,2);
    QString humidity    = msg.section('=',3,3);
    QString disdance    = msg.section('=',4,4);

    qDebug()<<"日期"<<date;
    qDebug()<<"时间"<<time;
    qDebug()<<"温度"<<temperature;
    qDebug()<<"湿度"<<humidity;
    qDebug()<<"距离"<<disdance;

    int year       = date.section('-',0,0).toInt();
    int mouth      = date.section('-',1,1).toInt();
    int day        = date.section('-',2,2).toInt();

    QString table_name = QString("tb_%1_%2_%3").arg(year).arg(mouth).arg(day);

    /*替换字符串中一些特定的字符串*/
    QString hour_value = "时";
    QString minute_value = "分";
    QString seconds_value = "秒";
    //第一个参数是指定替换字符串前半部分所有字符串长度，第二个参数是所替换字符串大小，新的字符串内容
    time.replace(2,1,hour_value);
    time.replace(5,1,minute_value);
    time.replace(8,1,seconds_value);
    qDebug()<<"time"<<time;

    //定义一个QSqlQuery的对象
    QSqlQuery sql_query;
    //判断数据库中是否已经存在此表
    if(!database.tables().contains(QString("%1").arg(table_name)))
    {
            //创建一个名为Data1的表格，表格包含三列，第一列是时间time，第二列是温度temperature，第三个是湿度humidity,varchar实际长度在变化
            QString create_sql = QString("create table %1 (time varchar(50),temperature varchar(50),humidity varchar(50),disdance varchar(50))")
                    .arg(table_name);//注意表名不能以数字开头，用“_“隔开
            qDebug()<<"create_sql"<<create_sql;
           //将指令以QString类型通过prepare函数保存在QSqlQuery对象中
            sql_query.prepare(create_sql);
            //执行创建表指令
           if(!sql_query.exec())
           {
               qDebug()<<"Error: Fail to create table."<<sql_query.lastError();
           }
           else
           {
               qDebug()<<" Table create successfully";
           }
    }

    QString insert_sql = QString("INSERT INTO %1 VALUES('%2',%3,%4,%5)")//有中文字符需要加单引号
           .arg(table_name)
           .arg(time)
           .arg(temperature)
           .arg(humidity)
           .arg(disdance);
   qDebug()<<"insert_sql"<<insert_sql;
   if(!sql_query.exec(insert_sql))
   {
       qDebug()<<sql_query.lastError();
   }
   else
   {
       qDebug()<<"inserted successfully";
   }
}

/*当客户端掉线的时候会调用此函数*/
void TcpServer::client_dis()
{
    //信号发送者--掉线客户端套接字
    QTcpSocket *sock = (QTcpSocket*)sender();
    //从sockets列表中找到套接字所在的位置
    int row = sockets.indexOf(sock);
    //根据行从ip列表中删除Ip
    QListWidgetItem *item  = ui->listWidget->takeItem(row);
    delete item;
    //把掉线套接字从列表sockets中删除
    sockets.removeOne(sock);
    if(ui->listWidget->count() == 0)
        ui->sendBt->setEnabled(false);
}

/*跳转到登陆界面*/
void TcpServer::on_cancelBt_clicked()
{
    //注销
    LoginWinserver *w = new LoginWinserver();
    w->show();
    this->close();
}

/*跳转到数据显示设置界面*/
void TcpServer::on_view_databaseBt_clicked()
{   //跳转到数据显示
    DatabaseShow *w = new DatabaseShow(this);
    w->show();
    this->hide();
}
