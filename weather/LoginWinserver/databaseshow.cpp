#include "databaseshow.h"
#include "ui_databaseshow.h"
#include "loginwinserver.h"
#include "tcpserver.h"
#include <QStringListIterator>
#include <QMenu>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QCursor>
#include <QSqlTableModel>

extern QSqlDatabase database;

DatabaseShow::DatabaseShow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DatabaseShow)
{
    ui->setupUi(this);
    ui->listWidget->setContextMenuPolicy(Qt::CustomContextMenu );

    ui->tableView->hide();
}

DatabaseShow::~DatabaseShow()
{
    delete ui;
}
/*显示数据库中所有表格数据*/
void DatabaseShow::on_Display_allBt_clicked()
{
 if(database.open())
 {
     qDebug()<<"open database success";
     QStringList  table = database.tables();
     qDebug()<<QString::fromLocal8Bit(" Number of tables:%1").arg(table.count());
     ui->label->setText(QString::fromLocal8Bit("Number %1 ").arg(table.count()));
     QStringListIterator itr(table);
     while(itr.hasNext())
     {
         QString tableName = itr.next().toLocal8Bit();
         qDebug()<<QString::fromLocal8Bit("table_name:")+ tableName;
        ui->listWidget->addItem(tableName);
     }
 }
 else
    {
        qDebug() << "Open database failed!";
    }
}

/*单击右键出现选项菜单*/
void DatabaseShow::on_listWidget_customContextMenuRequested(const QPoint &pos)
{
    QListWidgetItem* curItem = ui->listWidget->itemAt(pos);
    if(curItem == NULL)
    {
        return;
    }
    QMenu *popMenu = new QMenu(this);
    QAction *deleteSeed = new QAction(tr("删除"),this);
     QAction *clearSeed = new QAction(tr("清空"),this);
     popMenu->addAction(deleteSeed);
     popMenu->addAction(clearSeed);
     connect(deleteSeed,SIGNAL(triggered()),this,SLOT(deleteSeedslot()));
     connect(clearSeed,SIGNAL(triggered()),this,SLOT(clearSeedslot()));
     popMenu->exec(QCursor::pos());
     delete popMenu;
     delete deleteSeed;
     delete clearSeed;
}

/*删除单条表格*/
void DatabaseShow::deleteSeedslot()
{
    int ch = QMessageBox::warning(NULL, "警告",
                                      "确定删除此条表格 ?",
                                      QMessageBox::Yes | QMessageBox::No,
                                      QMessageBox::No);
    if(ch != QMessageBox::Yes)
    {
        return;
    }
    else
   {
        int currentrow = ui->listWidget->currentRow();
        QString table_name = ui->listWidget->item(currentrow)->text();
        qDebug()<<"currentrow"<<currentrow;
        qDebug()<<"table_name"<<table_name;
        if(database.open())
        {
            QSqlQuery query;
            qDebug()<<"open database success";
            query.exec(QString("DROP TABLE %1").arg(table_name));
            if(!database.tables().contains(QString("%1").arg(table_name)))
            {
                QMessageBox  warnning;
                     warnning.setText("删除成功!");
                     warnning.exec();
            }
            else
            {
                QMessageBox  warnning;
                     warnning.setText("删除失败!");
                     warnning.exec();
            }
        }
    }
}
/*清空整个数据库*/
void DatabaseShow::clearSeedslot()
{
    int ch = QMessageBox::warning(NULL, "警告",
                                      "确定清空此数据库 ?",
                                      QMessageBox::Yes | QMessageBox::No,
                                      QMessageBox::No);
    if(ch != QMessageBox::Yes)
    {
        return;
    }
    else
    {
        QStringList  table = database.tables();
        QSqlQuery query;
        qDebug()<<QString::fromLocal8Bit(" Number of tables:%1").arg(table.count());
        int num = QString::fromLocal8Bit(" Number of tables:%1").arg(table.count()).toInt();
        int i;
        for(i=0;i<=num+1;i++)
        {
             QString table_name = ui->listWidget->item(i)->text();
             query.exec(QString("DROP TABLE %1").arg(table_name));
        }
        QMessageBox  warnning;
             warnning.setText("已全部清空!");
             warnning.exec();
    }

   ui->listWidget->clear();

}
/*双击显示该表格内容*/
void DatabaseShow::on_listWidget_doubleClicked(const QModelIndex &index)
{
      ui->tableView->show();
    QSqlTableModel *model = new QSqlTableModel;

    int currentrow = ui->listWidget->currentRow();
    QString table_name = ui->listWidget->item(currentrow)->text();
    qDebug()<<"currentrow"<<currentrow;
    qDebug()<<"table_name"<<table_name;
        model->setTable(table_name);
        model->setEditStrategy(QSqlTableModel::OnManualSubmit);
        model->select();
        ui->tableView->setModel(model);
        ui->tableView->show();
}

/*返回上一级*/
void DatabaseShow::on_backBt_clicked()
{
    //返回
    TcpServer *w =new TcpServer();
    w->show();
    this->hide();
}
/*注销用户*/
void DatabaseShow::on_exitBt_clicked()
{
    //注销
    LoginWinserver *w = new LoginWinserver();
    w->show();
    this->hide();
    database.close();
}
