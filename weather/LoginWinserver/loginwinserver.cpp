

#include "loginwinserver.h"
#include "ui_loginwinserver.h"
#include <QMessageBox>
#include "tcpserver.h"

LoginWinserver::LoginWinserver(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LoginWinserver)
{
    ui->setupUi(this);
}

LoginWinserver::~LoginWinserver()
{
    delete ui;
}

/*关闭登陆界面*/
void LoginWinserver::on_quitBt_2_clicked()
{
    //关闭窗口
    this->close();
}
/*用户登陆及页面跳转*/
void LoginWinserver::on_loginBt_2_clicked()
{
    QString pass = ui->passEdit_2->text();
    QString user = ui->userEdit_2->text();

    if(pass.isEmpty()  || user.isEmpty())
    {
        //提示框（消息盒子）
        QMessageBox::warning(this,"温馨提示","你忘了输入用户或密码");
        return ;
    }
    if(pass == "yuchao"  && user == "yuchao" )
    {
        //跳转到主界面
        TcpServer *w = new TcpServer();
        w->show();
        this->close();//如果后面要返回就隐藏，如果不需要返回就close
    }

}
