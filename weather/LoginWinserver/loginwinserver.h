#ifndef LOGINWINSERVER_H
#define LOGINWINSERVER_H

#include <QMainWindow>

namespace Ui {
class LoginWinserver;
}

class LoginWinserver : public QMainWindow
{
    Q_OBJECT
public:
    explicit LoginWinserver(QWidget *parent = 0);
    ~LoginWinserver();

private slots:
    void on_quitBt_2_clicked();
    void on_loginBt_2_clicked();

private:
    Ui::LoginWinserver *ui;
};

#endif // LOGINWINSERVER_H
