#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QMainWindow>
#include <QTcpServer>
#include <QTcpSocket>
#include <QList>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSql>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlDriver>

namespace Ui {
class TcpServer;
}

class TcpServer : public QMainWindow
{
    Q_OBJECT

public:
    explicit TcpServer(QWidget *parent = 0);
    ~TcpServer();

private slots:
    void on_startBt_clicked();
    void new_client();
    void on_sendBt_clicked();
    void read_data();
    void client_dis();

private:
    Ui::TcpServer *ui;
    QTcpServer* mserver;
    QTcpSocket* msocket;
    QSqlDatabase database;
    QList<QTcpSocket *> sockets;
   // QString time_curr;


};

#endif // TCPSERVER_H
