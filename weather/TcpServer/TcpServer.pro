#-------------------------------------------------
#
# Project created by QtCreator 2017-10-09T14:26:59
#
#-------------------------------------------------

QT       += core gui network
QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TcpServer
TEMPLATE = app


SOURCES += main.cpp\
        tcpserver.cpp

HEADERS  += tcpserver.h

FORMS    += tcpserver.ui
