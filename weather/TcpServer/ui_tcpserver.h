/********************************************************************************
** Form generated from reading UI file 'tcpserver.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TCPSERVER_H
#define UI_TCPSERVER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TcpServer
{
public:
    QPushButton *sendBt;
    QTextEdit *sendEdit;
    QListWidget *recvList;
    QPushButton *sendBt_2;
    QListWidget *listWidget;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QLineEdit *portEdit;
    QPushButton *startBt;
    QTextBrowser *textBrowser;
    QPushButton *pushButton;

    void setupUi(QWidget *TcpServer)
    {
        if (TcpServer->objectName().isEmpty())
            TcpServer->setObjectName(QStringLiteral("TcpServer"));
        TcpServer->resize(559, 418);
        sendBt = new QPushButton(TcpServer);
        sendBt->setObjectName(QStringLiteral("sendBt"));
        sendBt->setGeometry(QRect(470, 380, 75, 23));
        sendEdit = new QTextEdit(TcpServer);
        sendEdit->setObjectName(QStringLiteral("sendEdit"));
        sendEdit->setGeometry(QRect(190, 260, 361, 111));
        recvList = new QListWidget(TcpServer);
        recvList->setObjectName(QStringLiteral("recvList"));
        recvList->setGeometry(QRect(230, 10, 321, 121));
        sendBt_2 = new QPushButton(TcpServer);
        sendBt_2->setObjectName(QStringLiteral("sendBt_2"));
        sendBt_2->setGeometry(QRect(190, 380, 75, 23));
        listWidget = new QListWidget(TcpServer);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(0, 40, 181, 371));
        listWidget->setStyleSheet(QStringLiteral("background-color: rgb(170, 170, 255);"));
        layoutWidget = new QWidget(TcpServer);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 0, 181, 31));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        portEdit = new QLineEdit(layoutWidget);
        portEdit->setObjectName(QStringLiteral("portEdit"));

        horizontalLayout->addWidget(portEdit);

        startBt = new QPushButton(layoutWidget);
        startBt->setObjectName(QStringLiteral("startBt"));

        horizontalLayout->addWidget(startBt);

        textBrowser = new QTextBrowser(TcpServer);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(235, 141, 311, 121));
        pushButton = new QPushButton(TcpServer);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(320, 380, 80, 20));

        retranslateUi(TcpServer);

        QMetaObject::connectSlotsByName(TcpServer);
    } // setupUi

    void retranslateUi(QWidget *TcpServer)
    {
        TcpServer->setWindowTitle(QApplication::translate("TcpServer", "TcpServer", 0));
        sendBt->setText(QApplication::translate("TcpServer", "\345\217\221\351\200\201", 0));
        sendBt_2->setText(QApplication::translate("TcpServer", "\346\270\205\351\231\244", 0));
        portEdit->setPlaceholderText(QApplication::translate("TcpServer", "\347\253\257\345\217\243\345\217\267", 0));
        startBt->setText(QApplication::translate("TcpServer", "\346\211\223\345\274\200", 0));
        pushButton->setText(QApplication::translate("TcpServer", "\346\237\245\347\234\213\346\225\260\346\215\256\345\272\223", 0));
    } // retranslateUi

};

namespace Ui {
    class TcpServer: public Ui_TcpServer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TCPSERVER_H
