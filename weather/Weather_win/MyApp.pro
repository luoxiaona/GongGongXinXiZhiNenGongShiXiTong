#-------------------------------------------------
#
# Project created by QtCreator 2017-10-12T11:05:57
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyApp
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    inbutton.cpp \
    softkey.cpp \
    qextserialbase.cpp \
    qextserialport.cpp \
    win_qextserialport.cpp

HEADERS  += mainwindow.h \
    inbutton.h \
    softkey.h \
    qextserialbase.h \
    qextserialport.h \
    win_qextserialport.h

FORMS    += mainwindow.ui \
    softkey.ui
