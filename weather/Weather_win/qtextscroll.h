﻿#ifndef QTEXTSCROLL_H
#define QTEXTSCROLL_H

#include <QMainWindow>
#include<QLabel>

namespace Ui {
class QTextScroll;
}

class QTextScroll : public QLabel
{
    Q_OBJECT

public:
    explicit QTextScroll(QWidget *parent = 0);
      void showchartext(QString text);
    ~QTextScroll();
 private slots:
      void updateindex();

private:
    Ui::QTextScroll *ui;
    int Width;
    int time;
    int curindex;
    QString showtext;
    QTimer *chartimer;

protected:
   void paintEvent(QPaintEvent *event);

};

#endif // QTEXTSCROLL_H
