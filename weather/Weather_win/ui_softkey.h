/********************************************************************************
** Form generated from reading UI file 'softkey.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SOFTKEY_H
#define UI_SOFTKEY_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>
#include <inbutton.h>

QT_BEGIN_NAMESPACE

class Ui_SoftKey
{
public:
    InButton *toolButton_25;
    InButton *toolButton_26;
    InButton *toolButton_27;
    InButton *toolButton_8;
    InButton *toolButton_9;
    InButton *toolButton_12;
    InButton *toolButton_10;
    InButton *toolButton_7;
    InButton *toolButton_6;
    InButton *toolButton_11;
    InButton *toolButton_4;
    InButton *toolButton_14;
    InButton *toolButton_13;
    InButton *toolButton;
    InButton *toolButton_5;
    InButton *toolButton_2;
    InButton *toolButton_3;
    InButton *toolButton_22;
    InButton *toolButton_17;
    InButton *toolButton_16;
    InButton *toolButton_15;
    InButton *toolButton_24;
    InButton *toolButton_20;
    InButton *toolButton_23;
    InButton *toolButton_21;
    InButton *toolButton_18;
    InButton *toolButton_19;
    QPushButton *chBt;
    QPushButton *pushButton_2;
    QToolButton *toolButton_28;

    void setupUi(QWidget *SoftKey)
    {
        if (SoftKey->objectName().isEmpty())
            SoftKey->setObjectName(QStringLiteral("SoftKey"));
        SoftKey->resize(246, 138);
        SoftKey->setMinimumSize(QSize(0, 0));
        SoftKey->setStyleSheet(QLatin1String("\n"
"background-color: rgb(170, 255, 255);"));
        toolButton_25 = new InButton(SoftKey);
        toolButton_25->setObjectName(QStringLiteral("toolButton_25"));
        toolButton_25->setGeometry(QRect(215, 71, 25, 25));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(toolButton_25->sizePolicy().hasHeightForWidth());
        toolButton_25->setSizePolicy(sizePolicy);
        toolButton_25->setMinimumSize(QSize(25, 25));
        toolButton_26 = new InButton(SoftKey);
        toolButton_26->setObjectName(QStringLiteral("toolButton_26"));
        toolButton_26->setGeometry(QRect(9, 102, 25, 27));
        sizePolicy.setHeightForWidth(toolButton_26->sizePolicy().hasHeightForWidth());
        toolButton_26->setSizePolicy(sizePolicy);
        toolButton_26->setMinimumSize(QSize(25, 25));
        toolButton_27 = new InButton(SoftKey);
        toolButton_27->setObjectName(QStringLiteral("toolButton_27"));
        toolButton_27->setGeometry(QRect(38, 102, 25, 27));
        sizePolicy.setHeightForWidth(toolButton_27->sizePolicy().hasHeightForWidth());
        toolButton_27->setSizePolicy(sizePolicy);
        toolButton_27->setMinimumSize(QSize(25, 25));
        toolButton_8 = new InButton(SoftKey);
        toolButton_8->setObjectName(QStringLiteral("toolButton_8"));
        toolButton_8->setGeometry(QRect(95, 71, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_8->sizePolicy().hasHeightForWidth());
        toolButton_8->setSizePolicy(sizePolicy);
        toolButton_8->setMinimumSize(QSize(25, 25));
        toolButton_8->setStyleSheet(QStringLiteral(""));
        toolButton_9 = new InButton(SoftKey);
        toolButton_9->setObjectName(QStringLiteral("toolButton_9"));
        toolButton_9->setGeometry(QRect(124, 71, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_9->sizePolicy().hasHeightForWidth());
        toolButton_9->setSizePolicy(sizePolicy);
        toolButton_9->setMinimumSize(QSize(25, 25));
        toolButton_12 = new InButton(SoftKey);
        toolButton_12->setObjectName(QStringLiteral("toolButton_12"));
        toolButton_12->setGeometry(QRect(184, 71, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_12->sizePolicy().hasHeightForWidth());
        toolButton_12->setSizePolicy(sizePolicy);
        toolButton_12->setMinimumSize(QSize(25, 25));
        toolButton_10 = new InButton(SoftKey);
        toolButton_10->setObjectName(QStringLiteral("toolButton_10"));
        toolButton_10->setGeometry(QRect(155, 71, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_10->sizePolicy().hasHeightForWidth());
        toolButton_10->setSizePolicy(sizePolicy);
        toolButton_10->setMinimumSize(QSize(25, 25));
        toolButton_7 = new InButton(SoftKey);
        toolButton_7->setObjectName(QStringLiteral("toolButton_7"));
        toolButton_7->setGeometry(QRect(140, 102, 32, 19));
        sizePolicy.setHeightForWidth(toolButton_7->sizePolicy().hasHeightForWidth());
        toolButton_7->setSizePolicy(sizePolicy);
        toolButton_6 = new InButton(SoftKey);
        toolButton_6->setObjectName(QStringLiteral("toolButton_6"));
        toolButton_6->setGeometry(QRect(67, 71, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_6->sizePolicy().hasHeightForWidth());
        toolButton_6->setSizePolicy(sizePolicy);
        toolButton_6->setMinimumSize(QSize(25, 25));
        toolButton_11 = new InButton(SoftKey);
        toolButton_11->setObjectName(QStringLiteral("toolButton_11"));
        toolButton_11->setGeometry(QRect(38, 71, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_11->sizePolicy().hasHeightForWidth());
        toolButton_11->setSizePolicy(sizePolicy);
        toolButton_11->setMinimumSize(QSize(25, 25));
        toolButton_4 = new InButton(SoftKey);
        toolButton_4->setObjectName(QStringLiteral("toolButton_4"));
        toolButton_4->setGeometry(QRect(215, 40, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_4->sizePolicy().hasHeightForWidth());
        toolButton_4->setSizePolicy(sizePolicy);
        toolButton_4->setMinimumSize(QSize(25, 25));
        toolButton_14 = new InButton(SoftKey);
        toolButton_14->setObjectName(QStringLiteral("toolButton_14"));
        toolButton_14->setGeometry(QRect(95, 40, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_14->sizePolicy().hasHeightForWidth());
        toolButton_14->setSizePolicy(sizePolicy);
        toolButton_14->setMinimumSize(QSize(25, 25));
        toolButton_13 = new InButton(SoftKey);
        toolButton_13->setObjectName(QStringLiteral("toolButton_13"));
        toolButton_13->setGeometry(QRect(67, 40, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_13->sizePolicy().hasHeightForWidth());
        toolButton_13->setSizePolicy(sizePolicy);
        toolButton_13->setMinimumSize(QSize(25, 25));
        toolButton = new InButton(SoftKey);
        toolButton->setObjectName(QStringLiteral("toolButton"));
        toolButton->setGeometry(QRect(124, 40, 25, 25));
        sizePolicy.setHeightForWidth(toolButton->sizePolicy().hasHeightForWidth());
        toolButton->setSizePolicy(sizePolicy);
        toolButton->setMinimumSize(QSize(25, 25));
        toolButton_5 = new InButton(SoftKey);
        toolButton_5->setObjectName(QStringLiteral("toolButton_5"));
        toolButton_5->setGeometry(QRect(9, 71, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_5->sizePolicy().hasHeightForWidth());
        toolButton_5->setSizePolicy(sizePolicy);
        toolButton_5->setMinimumSize(QSize(25, 25));
        toolButton_2 = new InButton(SoftKey);
        toolButton_2->setObjectName(QStringLiteral("toolButton_2"));
        toolButton_2->setGeometry(QRect(155, 40, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_2->sizePolicy().hasHeightForWidth());
        toolButton_2->setSizePolicy(sizePolicy);
        toolButton_2->setMinimumSize(QSize(25, 25));
        toolButton_3 = new InButton(SoftKey);
        toolButton_3->setObjectName(QStringLiteral("toolButton_3"));
        toolButton_3->setGeometry(QRect(184, 40, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_3->sizePolicy().hasHeightForWidth());
        toolButton_3->setSizePolicy(sizePolicy);
        toolButton_3->setMinimumSize(QSize(25, 25));
        toolButton_22 = new InButton(SoftKey);
        toolButton_22->setObjectName(QStringLiteral("toolButton_22"));
        toolButton_22->setGeometry(QRect(9, 9, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_22->sizePolicy().hasHeightForWidth());
        toolButton_22->setSizePolicy(sizePolicy);
        toolButton_22->setMinimumSize(QSize(25, 25));
        toolButton_22->setMaximumSize(QSize(16777215, 16777215));
        toolButton_17 = new InButton(SoftKey);
        toolButton_17->setObjectName(QStringLiteral("toolButton_17"));
        toolButton_17->setGeometry(QRect(9, 40, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_17->sizePolicy().hasHeightForWidth());
        toolButton_17->setSizePolicy(sizePolicy);
        toolButton_17->setMinimumSize(QSize(25, 25));
        toolButton_17->setMaximumSize(QSize(16777215, 16777215));
        toolButton_16 = new InButton(SoftKey);
        toolButton_16->setObjectName(QStringLiteral("toolButton_16"));
        toolButton_16->setGeometry(QRect(124, 9, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_16->sizePolicy().hasHeightForWidth());
        toolButton_16->setSizePolicy(sizePolicy);
        toolButton_16->setMinimumSize(QSize(25, 25));
        toolButton_16->setMaximumSize(QSize(16777215, 16777215));
        toolButton_15 = new InButton(SoftKey);
        toolButton_15->setObjectName(QStringLiteral("toolButton_15"));
        toolButton_15->setGeometry(QRect(215, 9, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_15->sizePolicy().hasHeightForWidth());
        toolButton_15->setSizePolicy(sizePolicy);
        toolButton_15->setMinimumSize(QSize(25, 25));
        toolButton_15->setMaximumSize(QSize(16777215, 16777215));
        toolButton_24 = new InButton(SoftKey);
        toolButton_24->setObjectName(QStringLiteral("toolButton_24"));
        toolButton_24->setGeometry(QRect(38, 40, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_24->sizePolicy().hasHeightForWidth());
        toolButton_24->setSizePolicy(sizePolicy);
        toolButton_24->setMinimumSize(QSize(25, 25));
        toolButton_24->setMaximumSize(QSize(16777215, 16777215));
        toolButton_20 = new InButton(SoftKey);
        toolButton_20->setObjectName(QStringLiteral("toolButton_20"));
        toolButton_20->setGeometry(QRect(155, 9, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_20->sizePolicy().hasHeightForWidth());
        toolButton_20->setSizePolicy(sizePolicy);
        toolButton_20->setMinimumSize(QSize(25, 25));
        toolButton_20->setMaximumSize(QSize(16777215, 16777215));
        toolButton_23 = new InButton(SoftKey);
        toolButton_23->setObjectName(QStringLiteral("toolButton_23"));
        toolButton_23->setGeometry(QRect(67, 9, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_23->sizePolicy().hasHeightForWidth());
        toolButton_23->setSizePolicy(sizePolicy);
        toolButton_23->setMinimumSize(QSize(25, 25));
        toolButton_23->setMaximumSize(QSize(16777215, 16777215));
        toolButton_21 = new InButton(SoftKey);
        toolButton_21->setObjectName(QStringLiteral("toolButton_21"));
        toolButton_21->setGeometry(QRect(38, 9, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_21->sizePolicy().hasHeightForWidth());
        toolButton_21->setSizePolicy(sizePolicy);
        toolButton_21->setMinimumSize(QSize(25, 25));
        toolButton_21->setMaximumSize(QSize(16777215, 16777215));
        toolButton_18 = new InButton(SoftKey);
        toolButton_18->setObjectName(QStringLiteral("toolButton_18"));
        toolButton_18->setGeometry(QRect(184, 9, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_18->sizePolicy().hasHeightForWidth());
        toolButton_18->setSizePolicy(sizePolicy);
        toolButton_18->setMinimumSize(QSize(25, 25));
        toolButton_18->setMaximumSize(QSize(16777215, 16777215));
        toolButton_19 = new InButton(SoftKey);
        toolButton_19->setObjectName(QStringLiteral("toolButton_19"));
        toolButton_19->setGeometry(QRect(95, 9, 25, 25));
        sizePolicy.setHeightForWidth(toolButton_19->sizePolicy().hasHeightForWidth());
        toolButton_19->setSizePolicy(sizePolicy);
        toolButton_19->setMinimumSize(QSize(25, 25));
        toolButton_19->setMaximumSize(QSize(16777215, 16777215));
        chBt = new QPushButton(SoftKey);
        chBt->setObjectName(QStringLiteral("chBt"));
        chBt->setGeometry(QRect(95, 102, 30, 30));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(23);
        sizePolicy1.setVerticalStretch(23);
        sizePolicy1.setHeightForWidth(chBt->sizePolicy().hasHeightForWidth());
        chBt->setSizePolicy(sizePolicy1);
        chBt->setMinimumSize(QSize(30, 30));
        chBt->setMaximumSize(QSize(30, 30));
        pushButton_2 = new QPushButton(SoftKey);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(200, 102, 30, 30));
        sizePolicy.setHeightForWidth(pushButton_2->sizePolicy().hasHeightForWidth());
        pushButton_2->setSizePolicy(sizePolicy);
        pushButton_2->setMinimumSize(QSize(30, 30));
        pushButton_2->setMaximumSize(QSize(30, 30));
        pushButton_2->setStyleSheet(QLatin1String("background-color: rgb(96, 96, 143);\n"
"color: rgb(226, 226, 226);"));
        toolButton_28 = new QToolButton(SoftKey);
        toolButton_28->setObjectName(QStringLiteral("toolButton_28"));
        toolButton_28->setGeometry(QRect(67, 102, 30, 30));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(toolButton_28->sizePolicy().hasHeightForWidth());
        toolButton_28->setSizePolicy(sizePolicy2);
        toolButton_28->setMinimumSize(QSize(30, 30));
        toolButton_28->setMaximumSize(QSize(30, 30));

        retranslateUi(SoftKey);

        QMetaObject::connectSlotsByName(SoftKey);
    } // setupUi

    void retranslateUi(QWidget *SoftKey)
    {
        SoftKey->setWindowTitle(QApplication::translate("SoftKey", "Form", 0));
        toolButton_25->setText(QApplication::translate("SoftKey", "b", 0));
        toolButton_26->setText(QApplication::translate("SoftKey", "n", 0));
        toolButton_27->setText(QApplication::translate("SoftKey", "m", 0));
        toolButton_8->setText(QApplication::translate("SoftKey", "z", 0));
        toolButton_9->setText(QApplication::translate("SoftKey", "x", 0));
        toolButton_12->setText(QApplication::translate("SoftKey", "v", 0));
        toolButton_10->setText(QApplication::translate("SoftKey", "c", 0));
        toolButton_7->setText(QApplication::translate("SoftKey", "<<", 0));
        toolButton_6->setText(QApplication::translate("SoftKey", "l", 0));
        toolButton_11->setText(QApplication::translate("SoftKey", "k", 0));
        toolButton_4->setText(QApplication::translate("SoftKey", "h", 0));
        toolButton_14->setText(QApplication::translate("SoftKey", "s", 0));
        toolButton_13->setText(QApplication::translate("SoftKey", "a", 0));
        toolButton->setText(QApplication::translate("SoftKey", "d", 0));
        toolButton_5->setText(QApplication::translate("SoftKey", "j", 0));
        toolButton_2->setText(QApplication::translate("SoftKey", "f", 0));
        toolButton_3->setText(QApplication::translate("SoftKey", "g", 0));
        toolButton_22->setText(QApplication::translate("SoftKey", "t", 0));
        toolButton_17->setText(QApplication::translate("SoftKey", "w", 0));
        toolButton_16->setText(QApplication::translate("SoftKey", "o", 0));
        toolButton_15->setText(QApplication::translate("SoftKey", "p", 0));
        toolButton_24->setText(QApplication::translate("SoftKey", "i", 0));
        toolButton_20->setText(QApplication::translate("SoftKey", "u", 0));
        toolButton_23->setText(QApplication::translate("SoftKey", "y", 0));
        toolButton_21->setText(QApplication::translate("SoftKey", "q", 0));
        toolButton_18->setText(QApplication::translate("SoftKey", "e", 0));
        toolButton_19->setText(QApplication::translate("SoftKey", "r", 0));
        chBt->setText(QApplication::translate("SoftKey", "\344\270\255", 0));
        pushButton_2->setText(QApplication::translate("SoftKey", "\347\254\246\345\217\267", 0));
        toolButton_28->setText(QApplication::translate("SoftKey", "123", 0));
    } // retranslateUi

};

namespace Ui {
    class SoftKey: public Ui_SoftKey {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SOFTKEY_H
