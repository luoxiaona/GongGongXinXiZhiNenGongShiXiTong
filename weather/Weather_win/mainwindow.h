﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QTimer>
#include<QProcess>
#include <QTcpSocket>
#include "win_qextserialport.h"
#include <QFile>
#include<QLabel>
namespace Ui {

class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);


    ~MainWindow();
    void mouseReleaseEvent(QMouseEvent *e);

private slots:
   void read_data(QNetworkReply *reply);
   void on_lineEdit_selectionChanged();
   void update_time();
   void on_queryBt_clicked();
   void read_data_pic(QNetworkReply*);
   void read_pic(QNetworkReply*);
   void  request_pic();
   void read_msg();
   void on_videoBt_clicked();
   void connect_to_server();
   void disconnect_from_server();
   void scrollCaption();
   void wordtimer();
   void readMyCom();         //声明读串口槽函数
   void show_time();
   void scrollCaptionto();
   void wordtimerto();
   void visitors_flowrate();
  void sleep(unsigned int msec);

private:
    Ui::MainWindow *ui;
    QNetworkAccessManager *manager;
    QNetworkAccessManager *manager_pic;
    QNetworkAccessManager *downloadMg;
    QTimer *mtimer ;
    QProcess *mprocess;
    QTcpSocket *msocket;
    quint64 recvsize;
    quint64 filesize;
    QFile file;
    QTimer *timer;
    QString strScrollCation;
    Win_QextSerialPort *myCom; //声明对象
    QTimer *ntimer;
    QTimer *dtimer;
    QTimer *t;
    QFont wordfont;
    int witdth;
    int hight;
    int event=0;
    int i=0;
    int j=0;
    int dis_event=0;
    int dis_number=0;
    QString high;
    QString low;
    QString ch;

    QList<QString> picPaths;


};

#endif // MAINWINDOW_H
