﻿#include "qtextscroll.h"
#include "ui_qtextscroll.h"
#include<QTimer>
#include<QPainter>
#include<QObject>


QTextScroll::QTextScroll(QWidget *parent) :
    QLabel(parent),
    ui(new Ui::QTextScroll)
{
    time = 20;
    Width = 5;
    curindex = 0;

    chartimer = new QTimer;
    connect(chartimer,SIGNAL(timeout()), this, SLOT(updateIndex()));

}

QTextScroll::~QTextScroll()
{
    delete ui;
}
void QTextScroll::showchartext(QString text)
{
    if(chartimer->isActive())
        chartimer->stop();

   showtext= text;
    chartimer->start(time);
}

void QTextScroll::updateindex()
{
    update();
    curindex++;
    if(curindex*Width > width())
        curindex = 0;
}

void QTextScroll::paintEvent(QPaintEvent *event)
{
    QWidget::paintEvent(event);
    QPainter painter(this);
    painter.drawText(0-Width*curindex,100,showtext);
    painter.drawText(width() - Width*curindex,100,showtext.left(curindex));
}

