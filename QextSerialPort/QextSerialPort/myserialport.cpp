#include "myserialport.h"
#include "ui_myserialport.h"
#include "QDebug"
#include "QString"
#include "QByteArray"
#include<QTextCodec>

MyserialPort::MyserialPort(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MyserialPort)
{
    ui->setupUi(this);

   struct PortSettings myComSetting = {BAUD115200,DATA_8,PAR_NONE,STOP_1,FLOW_OFF,1000};
   //定义一个结构体，存放串口的各个参数，波特率、数据位、校验位、停止位等

   myCom = new Win_QextSerialPort("com6",myComSetting,QextSerialBase::EventDriven);
   //定义串口对象，并传递参数，在构造函数中对其进行初始化

   myCom->open(QIODevice::ReadWrite);
   //以可读可写的方式打开串口文件

   connect(myCom,SIGNAL(readyRead()),this,SLOT(readMyCom()));
   //信号和槽函数关联，当缓冲区有数据时，进行读串口操作

   ntimer = new QTimer();
   connect(ntimer,SIGNAL(timeout()),this,SLOT(show_time()));
}

MyserialPort::~MyserialPort()
{
    delete ui;
}

void MyserialPort::readMyCom()//读串口函数
{
    QByteArray temp = myCom->readAll();
    //读取串口缓冲区的所有数据给临时变量temp
     qDebug()<<temp;
     QString buf = QString(temp);
      qDebug()<<"buf="<<buf;
    ui->textBrowser->insertPlainText(temp);
    //将串口的数据显示在窗口的文本浏览器中

    QString::SectionFlag flag = QString::SectionSkipEmpty;
    QString temperature = buf.section('-',0,0);
    QString humidity    = buf.section('-',1,1);
    QString disdance    = buf.section('-',2,2);
    qDebug()<<"湿度"<<temperature;
    qDebug()<<"温度"<<humidity;
    qDebug()<<"距离"<<disdance;

}
void MyserialPort::show_time(void)
{


    QByteArray temp = myCom->readAll();
    //读取串口缓冲区的所有数据给临时变量temp
     qDebug()<<temp;
     QString buf = QString(temp);
      qDebug()<<"buf11="<<buf;

    ui->label->setText(buf);


}
