#ifndef MYSERIALPORT_H
#define MYSERIALPORT_H

#include <QMainWindow>
#include "win_qextserialport.h"
#include <QTimer>

namespace Ui {
class MyserialPort;
}

class MyserialPort : public QMainWindow
{
    Q_OBJECT

public:
    explicit MyserialPort(QWidget *parent = 0);
    ~MyserialPort();

private:
    Ui::MyserialPort *ui;
    Win_QextSerialPort *myCom; //声明对象
     QTimer *ntimer ;
 private slots:

    void readMyCom();         //声明读串口槽函数
    void show_time();
};

#endif // MYSERIALPORT_H
