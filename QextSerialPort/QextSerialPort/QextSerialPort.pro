#-------------------------------------------------
#
# Project created by QtCreator 2017-11-22T19:45:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QextSerialPort
TEMPLATE = app


SOURCES += main.cpp\
        myserialport.cpp \
    qextserialbase.cpp \
    qextserialport.cpp \
    win_qextserialport.cpp

HEADERS  += myserialport.h \
    qextserialbase.h \
    qextserialport.h \
    win_qextserialport.h

FORMS    += myserialport.ui
