/********************************************************************************
** Form generated from reading UI file 'myserialport.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYSERIALPORT_H
#define UI_MYSERIALPORT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyserialPort
{
public:
    QWidget *centralWidget;
    QTextBrowser *textBrowser;
    QLabel *label;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MyserialPort)
    {
        if (MyserialPort->objectName().isEmpty())
            MyserialPort->setObjectName(QStringLiteral("MyserialPort"));
        MyserialPort->resize(400, 300);
        centralWidget = new QWidget(MyserialPort);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        textBrowser = new QTextBrowser(centralWidget);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(10, 0, 161, 121));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(170, 10, 221, 41));
        MyserialPort->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MyserialPort);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 22));
        MyserialPort->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MyserialPort);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MyserialPort->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MyserialPort);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MyserialPort->setStatusBar(statusBar);

        retranslateUi(MyserialPort);

        QMetaObject::connectSlotsByName(MyserialPort);
    } // setupUi

    void retranslateUi(QMainWindow *MyserialPort)
    {
        MyserialPort->setWindowTitle(QApplication::translate("MyserialPort", "MyserialPort", 0));
        label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MyserialPort: public Ui_MyserialPort {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYSERIALPORT_H
